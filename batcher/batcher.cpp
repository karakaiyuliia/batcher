/*
file:		batcher.cpp
purpose:	batcher class implementation
author:		Yuliia Karakai
date:		25/09/2019
*/

#include "Batcher.hpp"

/** @fn void openConcurrently(vector<Batcher>& openSet)
	@brief opens a group of apps concurrently
	@return void
*/
void Batcher::openConcurrently(vector<Batcher>& openSet) {
	//loop through the vector of apps and open them concurrently
	for (Batcher& batcher : openSet) {

		cout << "launching " << batcher.filePath << "..." << endl;

		wstring application(batcher.filePath.begin(), batcher.filePath.end());
		wstring params(batcher.param.begin(), batcher.param.end());
		wstring command = application + L" " + params;
		STARTUPINFO sinfo = { 0 };
		sinfo.cb = sizeof(STARTUPINFO);		//cb count of bytes
		PROCESS_INFORMATION pi = { 0 };
		unsigned long const CP_MAX_COMMANDLINE = 32768;

		try {
			wchar_t* commandLine = new wchar_t[CP_MAX_COMMANDLINE];
			wcsncpy_s(commandLine, CP_MAX_COMMANDLINE, command.c_str(), command.size() + 1);

			//open app
			CreateProcess(NULL,
				commandLine,
				NULL,
				NULL,
				false,
				0,
				NULL,
				NULL,
				&sinfo,
				&pi
			);

			batcher.proc = pi.hProcess;
			delete[] commandLine;
		}
		catch (std::exception e) {
			wcerr << e.what() << endl;
		}
	}

	//get run times and exit codes and close all the handles
	vector<HANDLE> handles;
	for (Batcher b : openSet)
		handles.push_back(b.proc);

	WaitForMultipleObjects((DWORD)handles.size(), handles.data(), TRUE, INFINITE);

	for (Batcher& b : openSet)
		GetExitCodeProcess(b.proc, &b.exit);

	for (Batcher& b : openSet) {
		FILETIME createTime, exitTime, kernelTime, userTime;
		GetProcessTimes(b.proc, &createTime, &exitTime, &kernelTime, &userTime);
		SYSTEMTIME cst;
		FileTimeToSystemTime(&createTime, &cst);
		FileTimeToSystemTime(&kernelTime, &b.kernelT);
		FileTimeToSystemTime(&userTime, &b.userT);
	}

	for (Batcher b : openSet)
		CloseHandle(b.proc);
}

/** @fn void openSerially(vector<Batcher>& app)
	@brief opens apps one after another
	@return void
*/
void Batcher::openSerially(vector<Batcher>& app) {

	Batcher& batcher = app.front();

	for (Batcher& b : app) {
		cout << "launching " << b.filePath << "..." << endl;
	}

	wstring application(batcher.filePath.begin(), batcher.filePath.end());
	wstring params(batcher.param.begin(), batcher.param.end());
	wstring command = application + L" " + params;
	STARTUPINFO sinfo = { 0 };
	sinfo.cb = sizeof(STARTUPINFO);
	PROCESS_INFORMATION pi = { 0 };
	unsigned long const CP_MAX_COMMANDLINE = 32768;

	try {
		wchar_t* commandLine = new wchar_t[CP_MAX_COMMANDLINE];
		wcsncpy_s(commandLine, CP_MAX_COMMANDLINE, command.c_str(), command.size() + 1);

		//open app
		CreateProcess(NULL,
			commandLine,
			NULL,
			NULL,
			false,
			0,
			NULL,
			NULL,
			&sinfo,
			&pi
		);

		batcher.proc = pi.hProcess;
		delete[] commandLine;
	}
	catch (std::exception e) {
		wcerr << e.what() << endl;
	}

	WaitForSingleObject(batcher.proc, INFINITE);
	GetExitCodeProcess(batcher.proc, &batcher.exit);

	FILETIME createTime, exitTime, kernelTime, userTime;
	GetProcessTimes(batcher.proc, &createTime, &exitTime, &kernelTime, &userTime);
	SYSTEMTIME cst;
	FileTimeToSystemTime(&createTime, &cst);
	FileTimeToSystemTime(&kernelTime, &batcher.kernelT);
	FileTimeToSystemTime(&userTime, &batcher.userT);

	CloseHandle(batcher.proc);
}

/** @fn void getGroups(string filePath, map<int, vector<Batcher>>& group)
	@brief reads the file in and assign group numbers
	@return void
*/
void Batcher::getGroups(string filePath, map<int, vector<Batcher>>& group) {
	ifstream file(filePath.c_str());
	string line;
	string path;
	string cmdArgs;
	int appGroup = 0;
	while (getline(file, line)) {
		if (line == "") continue;
		Batcher batcher;
		std::map<int, vector<Batcher>>::iterator it;
		istringstream s(line);
		int paramNum = 0;
		string token;
		cmdArgs = "";

		//if there is something wrong, that line should be ignored
		while (getline(s, token, ',')) {
			if (paramNum == 0) {
				try {
					paramNum++;
					appGroup = stoi(trimString(token, true));
				}
				catch (exception) {
					goto outer;
				}
			}
			else if (paramNum == 1) {
				try {
					paramNum++;
					path = trimString(token, false);

					if (path.size() == 0)
						goto outer;
				}
				catch (exception) {
					goto outer;
				}
			}
			else if (paramNum == 2) {
				try {
					paramNum = 0;
					cmdArgs = trimString(token, false);
				}
				catch (exception) {
					goto outer;
				}
			}
		}

		batcher = Batcher(appGroup, path, cmdArgs);
		it = group.find(appGroup);

		if (it != group.end())
		{
			vector<Batcher> existingGroup = it->second;
			existingGroup.push_back(batcher);
			it->second = existingGroup;
		}
		else
		{
			vector<Batcher> newGroup;
			newGroup.push_back(batcher);
			group[appGroup] = newGroup;
		}

		outer:;
	}
}

/** @fn string Batcher::trimString(string s, bool allSpaces)
	@brief removes all whitespaces or just trailing and leading depending on allSpaces value
	@return string
*/
string Batcher::trimString(string s, bool allSpaces)
{
	if (allSpaces) {
		for (int i = 0; i < s.size(); ++i) {
			if (std::isspace(static_cast<unsigned char>(s[i])))
				s.erase(s.begin() + i);
		}
		return s;
	}
	else {
		if (s.empty())
			return s;

		//remove spaces
		std::size_t firstScan = s.find_first_not_of(' ');
		std::size_t first = firstScan == std::string::npos ? s.length() : firstScan;
		std::size_t lastSpace = s.find_last_not_of(' ');
		string noSpaces = s.substr(first, lastSpace - first + 1);

		//remove tabs
		std::size_t secondScan = s.find_first_not_of('\t');
		std::size_t second = secondScan == std::string::npos ? s.length() : secondScan;
		std::size_t lastTab = s.find_last_not_of('\t');

		return noSpaces.substr(second, lastTab - second + 1);
	}

	return s;

}

/** @fn vector<string> splitString(string value, char splitter)
	@brief splits the provided string when reaches the splitter
	@return vector<string>
*/
vector<string> Batcher::splitString(string value, char splitter) {
	try {
		stringstream path(value);
		string token;
		vector<std::string> seglist;

		while (std::getline(path, token, splitter))
		{
			seglist.push_back(token);
		}

		return seglist;
	}
	catch (exception e) { }
}

/** @fn void printReport(map<int, vector<Batcher>> group)
	@brief prints the report in tabular form to the original console
	@return void
*/
void Batcher::printReport(map<int, vector<Batcher>> group) {
	bool failed = false;
	cout << "\nSuccess:" << endl;
	cout << setw(1) << "group" << setw(15) << "kernel time" << setw(15) << "user time" << setw(17) << "exit code" << setw(20) << "program" << setw(40) << "cmd params" << endl;
	cout << setw(120) << setfill('-') << "-" << endl;
	map<int, vector<Batcher>>::iterator it;
	for (it = group.begin(); it != group.end(); it++) {
		for (Batcher b : it->second) {
			if (b.exit == 0) {
				cout << setw(1) << setfill(' ') << b.group << setw(15) << b.kernelT.wMilliseconds << setw(15) << b.userT.wMilliseconds << setw(15) << b.exit << setw(30) << splitString(b.filePath, '\\').back() << setw(35) << b.param << endl;
			}
			else {
				failed = true;
			}
		}
	}
	if (failed == true) {
		cout << endl << "Failure:" << endl;
		cout << setw(1) << "group" << setw(15) << "exit code" << setw(20) << "program" << setw(20) << "cmd params" << endl;
		cout << setw(60) << setfill('-') << "-" << endl;
		for (it = group.begin(); it != group.end(); it++) {
			for (Batcher b : it->second) {
				if (b.exit != 0) {
					cout << setw(1) << setfill(' ') << b.group << setw(15) << b.exit << setw(20) << splitString(b.filePath, '\\').back() << setw(20) << b.param << endl;
				}
			}
		}
	}
}