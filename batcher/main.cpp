/*
file:		main.cpp
purpose:	application and main function
author:		Yuliia Karakai
date:		20/09/2019
*/

#include "Batcher.hpp"
namespace fs = std::filesystem;

int main(int argc, char* argv[])
{
	Batcher b = Batcher();
	//validations
	if (argc != 2) {
		cerr << "Error: please check your arguments." << endl;
		return -1;
	}

	//store the file path
	istringstream iss(argv[1]);
	string filePath;
	iss >> filePath;

	//check if the file exists
	if (!fs::exists(filePath)) {
		cerr << "Error: no such file." << endl;
		return -1;
	}

	//Get Launch Groups From File
	map<int, vector<Batcher>> group;
	b.getGroups(filePath, group);

	//Launch Processes
	map<int, vector<Batcher>>::iterator launchIter;
	for (launchIter = group.begin(); launchIter != group.end(); launchIter++) {
		if (launchIter->second.size() > 1) {
			b.openConcurrently(launchIter->second);

		}
		else {
			b.openSerially(launchIter->second);
		}
	}

	b.printReport(group);
}