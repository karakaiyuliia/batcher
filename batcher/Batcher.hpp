#pragma once

/*
file:		Batcher.hpp
purpose:	batcher class declaration
author:		Yuliia Karakai
date:		25/09/2019
*/

#include <string>
#include <Windows.h>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>
#include <thread>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <algorithm> 
#include <functional>
using namespace std;

//a class that will hold neccessary information to launch apps
class Batcher {
public:
	Batcher() {}
	SYSTEMTIME kernelT;
	SYSTEMTIME userT;
	string filePath;
	string param;
	int group;
	HANDLE proc;
	long id;
	DWORD exit;

	Batcher(int group, string filePath, string param) : group(group), filePath(filePath), param(param), id(0), proc(0), exit(0) {}

	void openConcurrently(vector<Batcher>& launchGroup);
	void openSerially(vector<Batcher>& launchGroup);
	void getGroups(string filePath, map<int, vector<Batcher>>& group);
	void printReport(map<int, vector<Batcher>> launchGroups);
	vector<string> splitString(string value, char splitter);
	string trimString(string s, bool allSpaces);

};